function processSalaries(data) {
    if (!Array.isArray(data)) {
      console.log("Error: Data In Suffient.");
      return null;
    }
  
    for (let index = 0; index < data.length; index++) {
      let salary = parseFloat(data[index].salary.replace("$", ""));
      if (!isNaN(salary)) {
        let correctedSalary = salary * 10000;
        data[index].corrected_salary = correctedSalary;
      } else {
        console.log("Error: Invalid salary format.");
        return null;
      }
    }
  
    return data;
  }
  
  module.exports = processSalaries;
  