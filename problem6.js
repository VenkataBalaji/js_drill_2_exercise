function getAvgSalariesBasedOnCountry(data) {
    if (!Array.isArray(data)) {
      console.log("Error: Data Insuffient.");
      return null;
    }
  
    const averageSalaryByCountry = {};
    const countByCountry = {};
  
    for (let index = 0; index < data.length; index++) {
      let salary = parseFloat(data[index].salary.replace("$", ""));
      let country = data[index].location;
  
      if (!isNaN(salary)) {
        if (averageSalaryByCountry[country]) {
          averageSalaryByCountry[country] += salary;
          countByCountry[country]++;
        } else {
          averageSalaryByCountry[country] = salary;
          countByCountry[country] = 1;
        }
      } else {
        console.log("Error: Invalid salary format.");
        return null;
      }
    }
  
    for (const country in averageSalaryByCountry) {
      averageSalaryByCountry[country] /= countByCountry[country];
    }
  
    return averageSalaryByCountry;
  }
  
  module.exports = getAvgSalariesBasedOnCountry;
  