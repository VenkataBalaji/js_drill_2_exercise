//2. Convert all the salary values into proper numbers instead of strings.



function convertSalariesToNumber(data) {
    if (!Array.isArray(data)) {
      console.log("Error: Data In Suffient.");
      return null;
    }
  
    for (let index = 0; index < data.length; index++) {
      let salary = parseFloat(data[index].salary.replace("$", ""));
      if (!isNaN(salary)) {
        data[index].salary = salary;
      } else {
        console.log("Error: Invalid salary format.");
        return null;
      }
    }
  
    return data;
  }
  
  module.exports = convertSalariesToNumber;
  
