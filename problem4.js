function getSumOfSalaries(data) {
    if (!Array.isArray(data)) {
      console.log("Error: Data In Suffient.");
      return null;
    }
  
    let totalSalary = 0;
    for (let index = 0; index < data.length; index++) {
      let salary = parseFloat(data[index].salary.replace("$", ""));
      if (!isNaN(salary)) {
        totalSalary += salary;
      } else {
        console.log("Error: Invalid salary format.");
        return null;
      }
    }
  
    return totalSalary;
  }
  
  module.exports = getSumOfSalaries;
  