
//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

function findAllWebDevelopers(data) {
  if (!Array.isArray(data)) {
    return "Error: Data is not an array.";
  }

  let webDevelopers = [];
  for (let index = 0; index < data.length; index++) {
    let employee = data[index];
    if (employee.job.toLowerCase().includes("web developer")) {
      webDevelopers.push(employee);
    }
  }
  return webDevelopers;
}



module.exports=findAllWebDevelopers